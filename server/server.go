package server

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

type Server struct {
	ip     string
	client *http.Client
}

const (
	// Maximum payload to read for blobs.
	blobReadLimit = 1024 * 1024 * 5
)

// NewStatic returns a server connected to a specified IP.
func NewStatic(ip string) (*Server, error) {
	return &Server{
		ip:     ip,
		client: &http.Client{},
	}, nil
}

// SendControl sends a HTTP Get to the YamahaExtendedControl port.
func (s *Server) SendControl(ctx context.Context, cmd string) ([]byte, error) {
	path := fmt.Sprintf("http://%s/YamahaExtendedControl/%s", s.ip, cmd)

	req, err := http.NewRequestWithContext(ctx, "GET", path, nil)
	if err != nil {
		return nil, err
	}

	fmt.Printf("GET %s\n", path)
	resp, err := s.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	pl, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return pl, nil
}

func (s *Server) SendSubscribeUDP(ctx context.Context, port int) error {
	path := fmt.Sprintf("http://%s/YamahaExtendedControl/v1/system/getDeviceInfo", s.ip)

	req, err := http.NewRequestWithContext(ctx, "GET", path, nil)
	if err != nil {
		return err
	}
	req.Header.Add("X-AppName", "MusicCast/Yama(1.0)")
	req.Header.Add("X-AppPort", fmt.Sprintf("%d", port))
	resp, err := s.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	_, err = ioutil.ReadAll(resp.Body)
	return err
}

func (s *Server) SendUpnp(ctx context.Context, action string, payload []byte) error {
	path := fmt.Sprintf("http://%s:49154/AVTransport/ctrl", s.ip)

	req, err := http.NewRequestWithContext(ctx, "POST", path, bytes.NewReader(payload))
	if err != nil {
		return err
	}
	req.Header.Add(`Content-Type`, `text/xml; charset="utf-8"`)
	req.Header.Add(`SOAPAction`, fmt.Sprintf(`"urn:schemas-upnp-org:service:AVTransport:1#%s"`, action))
	resp, err := s.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	dbg, err := ioutil.ReadAll(resp.Body)
	fmt.Printf(">>> %+v\n>>>%+v\n", resp.Status, string(dbg))
	return err
}

func (s *Server) GetBlob(ctx context.Context, kind, key string) ([]byte, error) {
	path := fmt.Sprintf("http://%s/YamahaRemoteControl/%s/%s%s", s.ip, kind, kind, key)
	req, err := http.NewRequestWithContext(ctx, "GET", path, nil)
	if err != nil {
		return nil, err
	}
	resp, err := s.client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(io.LimitReader(resp.Body, blobReadLimit))
	if err != nil {
		return nil, err
	}
	if len(body) > 0 {
		return body, nil
	}
	return nil, fmt.Errorf("failed to fetch data")
}

// IP returns the current IP of the server.
func (s *Server) IP() string {
	return s.ip
}
