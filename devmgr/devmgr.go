package devmgr

import (
	"context"
	"sync"

	"codeberg.org/adrian-blx/ymc/controller"
	"codeberg.org/adrian-blx/ymc/monitor"
)

// dev is our internal structure holding information about YMC devices.
type dev struct {
	name  string
	id    string
	alive bool
	ctrl  *controller.Controller
}

type DevMgr struct {
	sync.RWMutex
	m []*dev
}

// New returns a new device manager which will scan for devices until context is done.
func New(ctx context.Context, devs []*controller.Controller, mon *monitor.Monitor) *DevMgr {
	dm := &DevMgr{}
	for _, d := range devs {
		dm.m = append(dm.m, &dev{ctrl: d})
	}
	go dm.runScan(ctx, mon)
	return dm
}

// AllOnlineDevices returns all devices we know about which are online.
func (dm *DevMgr) AllOnlineDevices() []string {
	dm.RLock()
	defer dm.RUnlock()

	var ids []string
	for _, d := range dm.m {
		if d.alive {
			ids = append(ids, d.id)
		}
	}
	return ids
}

// Device returns the device controller of the given id.
func (dm *DevMgr) Device(id string) (*controller.Controller, bool) {
	dm.RLock()
	defer dm.RUnlock()

	for _, d := range dm.m {
		if d.id == id {
			return d.ctrl, true
		}
	}
	return nil, false
}

// NameById returns the device name by the given id.
func (dm *DevMgr) NameById(id string) (string, bool) {
	dm.RLock()
	defer dm.RUnlock()

	for _, d := range dm.m {
		if d.id == id {
			return d.name, true
		}
	}
	return "", false
}
