package devmgr

import (
	"context"
	"fmt"
	"time"

	"codeberg.org/adrian-blx/ymc/monitor"
)

const (
	mainSleepTime = 4 * time.Minute
)

func (dm *DevMgr) runScan(ctx context.Context, mon *monitor.Monitor) {
	for ctx.Err() == nil {
		for _, d := range dm.m {
			_, err := d.ctrl.GetDeviceInfo(ctx)
			if err == nil && !d.alive {
				if err := dm.setDeviceAlive(ctx, mon, d); err != nil {
					fmt.Printf("setDeviceAlive error: %v\n", err)
				}
			}
			if err != nil && d.alive {
				if err := dm.setDeviceDead(ctx, mon, d); err != nil {
					fmt.Printf("setDeviceDead error: %v\n", err)
				}
			}
		}
		time.Sleep(mainSleepTime)
	}
}

func (dm *DevMgr) setDeviceAlive(ctx context.Context, mon *monitor.Monitor, d *dev) error {
	di, err := d.ctrl.GetDeviceInfo(ctx)
	if err != nil {
		return err
	}
	ni, err := d.ctrl.GetNetworkStatus(ctx)
	if err != nil {
		return err
	}

	if _, err := mon.AddDevice(ctx, d.ctrl); err != nil {
		return err
	}

	dm.Lock()
	defer dm.Unlock()
	d.id = di.DeviceId
	d.name = ni.NetworkName
	d.alive = true

	fmt.Printf("scan: device %q is now alive!\n", d.id)
	return nil
}

func (dm *DevMgr) setDeviceDead(ctx context.Context, mon *monitor.Monitor, d *dev) error {
	if err := mon.RemoveDevice(ctx, d.id); err != nil {
		return err
	}

	dm.Lock()
	defer dm.Unlock()
	d.alive = false

	fmt.Printf("scan: device %q is now DEAD\n", d.id)
	return nil
}
