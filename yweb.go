package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"sort"
	"strings"
	"time"

	"codeberg.org/adrian-blx/ymc/controller"
	"codeberg.org/adrian-blx/ymc/devmgr"
	"codeberg.org/adrian-blx/ymc/monitor"
	"codeberg.org/adrian-blx/ymc/server"
	"codeberg.org/adrian-blx/ymc/webui"
)

var (
	flagListen  = flag.String("listen", "127.0.0.1:8012", "IP:Port to listen on")
	flagUdp     = flag.Int("udp-listen", 8012, "IP:Port to listen on for UDP connecton")
	flagClients = flag.String("clients", "192.168.1.45,192.168.1.46", "comma separated list of devices")
	flagZone    = flag.String("zone", "main", "Zone to use")
	flagStreams = flag.String("streams", "", "Streams to hardcode")
)

func main() {
	flag.Parse()

	log.Printf("Listening on %q for HTTP, port %d for UDP\n", *flagListen, *flagUdp)

	var devs []*controller.Controller
	for _, ip := range strings.Split(*flagClients, ",") {
		log.Printf("Connecting to %q\n", ip)
		srv, err := server.NewStatic(ip)
		if err != nil {
			log.Fatalf("failed to create server for %q: %w\n", ip, err)
		}
		devs = append(devs, controller.New(*flagZone, srv))
	}
	log.Printf("Server knows about %d device(s)\n", len(devs))

	udp, err := net.ListenUDP("udp", &net.UDPAddr{Port: *flagUdp})
	if err != nil {
		log.Fatalf("failed to bind to UDP port %d: %w\n", *flagUdp, err)
	}

	ctx := context.Background()
	mon := monitor.Start(ctx, udp, *flagUdp)
	mgr := devmgr.New(ctx, devs, mon)
	mon.Subscribe(foo)

	s := &http.Server{
		Addr:           *flagListen,
		Handler:        webui.NewHandler(mgr, mon, buildStreams(*flagStreams)),
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	s.ListenAndServe()
}

func foo(did, ip string, info monitor.Status) {
	fmt.Printf(">>>> INFO<%s> <%s>: %+v\n", did, ip, info)
}

func buildStreams(raw string) []controller.PresetInfo {
	if len(raw) == 0 {
		return []controller.PresetInfo{}
	}

	streams := map[string]string{}
	if err := json.Unmarshal([]byte(raw), &streams); err != nil {
		log.Fatalf("failed to parse streams: %v", err)
	}

	var names []string
	for n := range streams {
		names = append(names, n)
	}
	sort.Strings(names)

	var res []controller.PresetInfo
	for _, k := range names {
		v := streams[k]
		res = append(res, controller.PresetInfo{
			Text: k,
			Url:  v,
		})
	}
	return res
}
