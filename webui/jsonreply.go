package webui

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

func jsonReply(w http.ResponseWriter, data interface{}) {
	pl, err := json.Marshal(data)
	if err != nil {
		log.Printf("failed to marshal data: %w\n", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
	fmt.Printf(">> %s\n", string(pl))

	w.Header().Add("Content-Type", "application/json; charset=utf-8")
	w.Write(pl)
}
