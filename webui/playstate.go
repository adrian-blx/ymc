package webui

import (
	"context"
	"encoding/json"
	"fmt"
	"hash/fnv"
	"log"
	"net/http"
	"time"

	"codeberg.org/adrian-blx/ymc/controller"
	"codeberg.org/adrian-blx/ymc/monitor"
)

// servePlayState returns the current play state, blocking if 'fp' matches what we would send.
func (h *Handler) servePlayState(ctx context.Context, w http.ResponseWriter, ctrl *controller.Controller, devID, knownFp string) {
	// Build a function which will return the events we receive (after subscribing), bound to the context.
	rctx, cancel := context.WithCancel(ctx)
	reply := make(chan *monitor.Status)
	fn := func(id, ip string, s monitor.Status) {
		if id != devID {
			return
		}
		fmt.Printf("Sending a reply: %+v\n", s)
		select {
		case reply <- &s:
			// nada
		case <-rctx.Done():
			log.Printf("servePlayState: dropping message, context done\n")
		default:
			log.Printf("servePlayState: dropping message: can't send\n")
		}
	}

	token, err := h.mon.Subscribe(fn)
	if err != nil {
		http.Error(w, "failed to subscribe", http.StatusInternalServerError)
		return
	}

	var jsonPayload []byte
	var newFp string
	for rctx.Err() == nil {
		select {
		case s := <-reply:
			// for each reply, assemble the json and calculate a fingerprint.
			// We will only return here if we got new data, unknown to the client.
			jsonPayload, _ = json.Marshal(s)
			h := fnv.New32a()
			h.Write(jsonPayload)
			newFp = fmt.Sprintf("fpv1_%d", h.Sum32())
			// new fingerprint is different, so we can send the reply right now.
			if newFp != knownFp {
				cancel()
			}
		case <-time.After(5 * time.Second):
			cancel()
			break
		}
	}

	// We are done: unsubscribe from events and drain channel.
	h.mon.Unsubscribe(token)
	for len(reply) > 0 {
		<-reply
	}
	close(reply)

	// We can finally reply, including the calculated fingerprint.
	w.Header().Add("X-Yama-Fingerprint", newFp)
	w.Header().Add("Content-Type", "application/json; charset=utf-8")
	w.Write(jsonPayload)
}
