package webui

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"codeberg.org/adrian-blx/ymc/controller"
)

func (h *Handler) setPlayback(ctx context.Context, w http.ResponseWriter, ctrl *controller.Controller, state string) {
	var err error
	switch state {
	case "pause":
		err = ctrl.SetPlayback(ctx, controller.PlaybackPause)
	case "play":
		err = ctrl.SetPlayback(ctx, controller.PlaybackPlay)
	case "stop":
		err = ctrl.SetPlayback(ctx, controller.PlaybackStop)
	case "previous":
		err = ctrl.SetPlayback(ctx, controller.PlaybackPrevious)
	case "next":
		err = ctrl.SetPlayback(ctx, controller.PlaybackNext)
	case "seek_back":
		err = ctrl.SetPlayback(ctx, controller.PlaybackSeekBackward)
	case "seek_fwd":
		err = ctrl.SetPlayback(ctx, controller.PlaybackSeekForward)
	default:
		err = fmt.Errorf("unknown playback state %q", state)
	}

	if err != nil {
		log.Printf("failed to set playback state: %w", err)
		http.Error(w, "invalid playback state", http.StatusInternalServerError)
		return
	}
}

func (h *Handler) setPlaybackRepeat(ctx context.Context, w http.ResponseWriter, ctrl *controller.Controller, mode string) {
	var err error
	switch mode {
	case "off":
		err = ctrl.SetRepeat(ctx, controller.RepeatOff)
	case "one":
		err = ctrl.SetRepeat(ctx, controller.RepeatOne)
	case "all":
		err = ctrl.SetRepeat(ctx, controller.RepeatAll)
	default:
		err = fmt.Errorf("unknown playback repeat mode %q", mode)
	}

	if err != nil {
		log.Printf("failed to set playback repeat mode: %w", err)
		http.Error(w, "invalid playback mode toggle", http.StatusInternalServerError)
		return
	}
}

func (h *Handler) setPlaybackShuffle(ctx context.Context, w http.ResponseWriter, ctrl *controller.Controller, mode string) {
	var err error
	switch mode {
	case "off":
		err = ctrl.SetShuffle(ctx, controller.ShuffleOff)
	case "on":
		err = ctrl.SetShuffle(ctx, controller.ShuffleOn)
	case "songs":
		err = ctrl.SetShuffle(ctx, controller.ShuffleSongs)
	case "albums":
		err = ctrl.SetShuffle(ctx, controller.ShuffleAlbums)
	default:
		err = fmt.Errorf("unknown playback shuffle mode %q", mode)
	}

	if err != nil {
		log.Printf("failed to set playback shuffle mode: %w", err)
		http.Error(w, "invalid playback mode toggle", http.StatusInternalServerError)
		return
	}
}
