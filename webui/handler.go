package webui

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"codeberg.org/adrian-blx/ymc/controller"
	"codeberg.org/adrian-blx/ymc/monitor"
)

type Handler struct {
	devmgr  DevManager
	mon     *monitor.Monitor
	streams []controller.PresetInfo
}

type DevManager interface {
	Device(string) (*controller.Controller, bool)
	AllOnlineDevices() []string
	NameById(string) (string, bool)
}

func NewHandler(devmgr DevManager, mon *monitor.Monitor, streams []controller.PresetInfo) *Handler {
	return &Handler{
		devmgr:  devmgr,
		mon:     mon,
		streams: streams,
	}
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/":
		serveIndex(w)
	case "/favicon.png":
		serveFavIcon(w)
	case "/devices.json":
		serveDevices(h.devmgr, w)
	default:
		goto NOT_YET_SERVED
	}
	return

NOT_YET_SERVED:
	// URLs served here need a controller.
	fmt.Printf("> %+v\n", r.URL.Path)
	devID := r.URL.Query().Get("devid")
	ctrl, ok := h.devmgr.Device(devID)
	if !ok {
		http.Error(w, "failed to find controller", http.StatusTeapot)
		return
	}

	ctx := context.Background()
	switch r.URL.Path {
	case "/devices.json":
		serveDevices(h.devmgr, w)
	case "/playstate.json":
		h.servePlayState(ctx, w, ctrl, devID, r.URL.Query().Get("fp"))
	case "/features.json":
		h.serveFeatures(ctx, w, ctrl)
	case "/favourites.json":
		h.serveFavourites(ctx, w, ctrl, r.URL.Query().Get("idx"))
	case "/listinfo.json":
		h.serveListInfo(ctx, w, ctrl, r.URL.Query().Get("input"), r.URL.Query().Get("action"), r.URL.Query().Get("idx"), r.URL.Query().Get("page"))
	case "/stream.json":
		h.serveStreamPresets(ctx, w, ctrl, r.URL.Query().Get("url"))
	case "/setpower.json":
		h.setPower(ctx, w, ctrl, r.URL.Query().Get("state"))
	case "/setinput.json":
		h.setInput(ctx, w, ctrl, r.URL.Query().Get("input"))
	case "/setvolume.json":
		h.setVolume(ctx, w, ctrl, r.URL.Query().Get("volume"))
	case "/setplayback.json":
		h.setPlayback(ctx, w, ctrl, r.URL.Query().Get("state"))
	case "/setshuffle.json":
		h.setPlaybackShuffle(ctx, w, ctrl, r.URL.Query().Get("mode"))
	case "/setrepeat.json":
		h.setPlaybackRepeat(ctx, w, ctrl, r.URL.Query().Get("mode"))
	case "/artwork.jpg":
		h.serveArtwork(ctx, w, ctrl, r.URL.Query().Get("artwork"))
	default:
		log.Printf("404 for %q", r.URL.Path)
		http.Error(w, "not found", http.StatusNotFound)
	}
}

func (h *Handler) UserDefinedStreamPresets() []controller.PresetInfo {
	return h.streams
}
