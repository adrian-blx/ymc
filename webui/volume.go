package webui

import (
	"context"
	"fmt"
	"net/http"
	"strconv"

	"codeberg.org/adrian-blx/ymc/controller"
)

func (h *Handler) setVolume(ctx context.Context, w http.ResponseWriter, ctrl *controller.Controller, action string) {
	var err error
	switch action {
	case "up":
		err = ctrl.VolumeChange(ctx, +1)
	case "down":
		err = ctrl.VolumeChange(ctx, -1)
	case "mute":
		err = ctrl.VolumeMute(ctx, true)
	case "unmute":
		err = ctrl.VolumeMute(ctx, false)
	default:
		var ok bool
		if len(action) > 2 && action[0] == '@' {
			var val int64
			val, err = strconv.ParseInt(action[1:], 10, 64)
			if err == nil {
				err = ctrl.VolumeSet(ctx, val)
				ok = true
			}
		}
		if !ok {
			err = fmt.Errorf("unknown action")
		}
	}

	if err != nil {
		http.Error(w, "unknown volume action", http.StatusInternalServerError)
		return
	}
}
