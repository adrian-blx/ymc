package webui

import (
	"context"
	"net/http"
	"strconv"

	"codeberg.org/adrian-blx/ymc/controller"
)

func (h *Handler) serveFavourites(ctx context.Context, w http.ResponseWriter, ctrl *controller.Controller, idx string) {
	num, _ := strconv.ParseInt(idx, 10, 64)
	if num < 0 {
		p, err := ctrl.GetPresets(ctx)
		if err != nil {
			http.Error(w, "failed to fetch presets", http.StatusInternalServerError)
			return
		}
		jsonReply(w, p)
		return
	}

	// else, switch to favourite.
	// Note that Yamaha uses 1-based indexes, we use 0 based in the json api.
	if err := ctrl.ActivatePreset(ctx, int(num)+1); err != nil {
		http.Error(w, "failed to enable preset", http.StatusInternalServerError)
	}
}

func (h *Handler) serveStreamPresets(ctx context.Context, w http.ResponseWriter, ctrl *controller.Controller, url string) {
	if url == "" {
		resp := controller.PresetContainer{
			ResponseCode: 200,
			PresetInfo:   h.UserDefinedStreamPresets(),
		}
		jsonReply(w, resp)
		return
	}
	if err := ctrl.SendUpnpUrl(ctx, url); err != nil {
		http.Error(w, "failed to set url", http.StatusInternalServerError)
		return
	}
	if err := ctrl.SendUpnpPlay(ctx); err != nil {
		http.Error(w, "failed to switch to upnp play", http.StatusInternalServerError)
		return
	}
}
