package webui

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"codeberg.org/adrian-blx/ymc/controller"
)

func (h *Handler) setPower(ctx context.Context, w http.ResponseWriter, ctrl *controller.Controller, state string) {
	fn := func(ctx context.Context, ctrl *controller.Controller) error {
		switch state {
		case "on":
			return ctrl.PowerOn(ctx)
		case "off":
			return ctrl.PowerOff(ctx)
		case "toggle":
			return ctrl.PowerToggle(ctx)
		default:
			return fmt.Errorf("invalid power state")
		}
	}

	h.setSimple(ctx, w, ctrl, fn)
}

func (h *Handler) setInput(ctx context.Context, w http.ResponseWriter, ctrl *controller.Controller, input string) {
	fn := func(ctx context.Context, ctrl *controller.Controller) error {
		i := controller.InputServer
		switch input {
		case "spotify":
			i = controller.InputSpotify
		case "tidal":
			i = controller.InputTidal
		case "deezer":
			i = controller.InputDeezer
		case "net_radio":
			i = controller.InputRadio
		case "server":
			i = controller.InputServer
		case "bluetooth":
			i = controller.InputBluetooth
		default:
			return fmt.Errorf("unknown input type")
		}
		ctrl.SetInput(ctx, i)
		return nil
	}
	h.setSimple(ctx, w, ctrl, fn)
}

func (h *Handler) setSimple(ctx context.Context, w http.ResponseWriter, ctrl *controller.Controller, fn func(context.Context, *controller.Controller) error) {
	if err := fn(ctx, ctrl); err != nil {
		log.Printf("error: %v\n", err)
		http.Error(w, "Internal error calling function", http.StatusInternalServerError)
	}
}
