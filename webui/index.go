package webui

import (
	_ "embed"
	"net/http"
)

var (
	//go:embed resources/index.gohtml
	indexPage []byte
	//go:embed resources/favicon.png
	favIcon []byte
)

func serveIndex(w http.ResponseWriter) {
	w.Write(indexPage)
}

func serveFavIcon(w http.ResponseWriter) {
	w.Write(favIcon)
}
