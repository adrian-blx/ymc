package webui

import (
	"net/http"
	"sort"

	"codeberg.org/adrian-blx/ymc/controller"
)

type Device struct {
	Controller *controller.Controller
	Name       string
}

type knownDeviceReply struct {
	DevID string `json:"devid"`
	Name  string `json:"name"`
}

func serveDevices(devmgr DevManager, w http.ResponseWriter) {
	var k []string
	for _, id := range devmgr.AllOnlineDevices() {
		k = append(k, id)
	}
	sort.Strings(k)

	var resp []knownDeviceReply
	for _, id := range k {
		name, ok := devmgr.NameById(id)
		if !ok {
			continue
		}
		resp = append(resp, knownDeviceReply{
			DevID: id,
			Name:  name,
		})
	}

	jsonReply(w, &resp)
}
