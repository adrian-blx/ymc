package webui

import (
	"context"
	"fmt"
	"net/http"
	"regexp"

	"codeberg.org/adrian-blx/ymc/controller"
)

var (
	reAlbumArt = regexp.MustCompile(`^/YamahaRemoteControl/[^/]+/([a-zA-Z]+)(\d+)\.([a-zA-Z]{3,4})$`)
)

func (h *Handler) serveArtwork(ctx context.Context, w http.ResponseWriter, ctrl *controller.Controller, artURL string) {
	matches := reAlbumArt.FindAllStringSubmatch(artURL, -1)
	if len(matches) != 1 {
		http.Error(w, "Failed to extract artwork params from payload", http.StatusNotFound)
		return
	}

	kind := matches[0][1]
	key := fmt.Sprintf("%s.%s", matches[0][2], matches[0][3])
	pl, err := ctrl.GetBlob(ctx, kind, key)
	if err != nil || len(pl) == 0 {
		http.Error(w, "Failed to fetch data", http.StatusInternalServerError)
		return
	}
	w.Write(pl)
}
