package webui

import (
	"context"
	"net/http"
	"strconv"

	"codeberg.org/adrian-blx/ymc/controller"
)

func (h *Handler) serveListInfo(ctx context.Context, w http.ResponseWriter, ctrl *controller.Controller, input, action, index, page string) {
	idx, _ := strconv.ParseInt(index, 10, 64)
	pg, _ := strconv.ParseInt(page, 10, 64)

	a := controller.ListControlPeek
	switch action {
	case "select":
		a = controller.ListControlSelect
	case "return":
		a = controller.ListControlReturn
	case "play":
		a = controller.ListControlPlay
	case "peek":
		a = controller.ListControlPeek
	default:
		http.Error(w, "invalid action", http.StatusInternalServerError)
		return
	}

	in := controller.InputRadio
	switch input {
	case "tidal":
		in = controller.InputTidal
	case "deezer":
		in = controller.InputDeezer
	case "net_radio":
		in = controller.InputRadio
	default:
		http.Error(w, "invalid input", http.StatusInternalServerError)
		return
	}

	reply, err := ctrl.ListInfo(ctx, a, int32(idx), int32(pg), in)
	if err != nil {
		http.Error(w, "failed to get list info", http.StatusInternalServerError)
		return
	}
	jsonReply(w, reply)
}
