package webui

import (
	"context"
	"net/http"

	"codeberg.org/adrian-blx/ymc/controller"
)

func (h *Handler) serveFeatures(ctx context.Context, w http.ResponseWriter, ctrl *controller.Controller) {
	f, err := ctrl.GetFeatures(ctx)
	if err != nil {
		http.Error(w, "Failed to fetch features", http.StatusInternalServerError)
		return
	}
	jsonReply(w, f)
}
