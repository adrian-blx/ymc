package monitor

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"sync"
	"time"

	"codeberg.org/adrian-blx/ymc/controller"
)

const (
	subscribeTtl = 4 * time.Minute
)

type DataSource interface {
	GetDeviceInfo(ctx context.Context) (*controller.DeviceInfo, error)
	GetStatus(ctx context.Context) (*controller.ZoneStatus, error)
	GetPlayInfo(ctx context.Context) (*controller.PlayInfo, error)
	SubscribeEvents(ctx context.Context, port int) error
	GetIP() string
}

type knownDevices struct {
	src    DataSource
	cancel context.CancelFunc
	status Status
}

type Monitor struct {
	sync.RWMutex
	conn             *net.UDPConn
	port             int
	devices          map[string]knownDevices
	eventSubscribers map[string]SubscribeFunc
	jsonChan         chan ([]byte)
}

// Status is the exported device status.
type Status struct {
	Power            string   `json:"power"`
	Input            string   `json:"input"`
	Mute             bool     `json:"mute"`
	Volume           uint32   `json:"volume"`
	Artist           string   `json:"artist"`
	Album            string   `json:"album"`
	Track            string   `json:"track"`
	Playback         string   `json:"playback"`
	PlayTime         int64    `json:"play_time"`
	Repeat           string   `json:"repeat"`
	RepeatAvailable  []string `json:"repeat_available"`
	Shuffle          string   `json:"shuffle"`
	ShuffleAvailable []string `json:"shuffle_available"`
	ArtworkURL       string   `json:"artwork_url"`
	SeekForwardStep  int64    `json:"seek_forward"`
	SeekBackwardStep int64    `json:"seek_backward"`
}

// udpDeviceStatus is a struct we will receive from the device using UDP.
type udpDeviceStatus struct {
	DeviceID string           `json:"device_id"`
	Main     *udpMainStatus   `json:"main"`
	NetUSB   *udpNetUSBStatus `json:"netusb"`
}

type udpMainStatus struct {
	Power  *string `json:"power"`
	Volume *uint32 `json:"volume"`
	Mute   *bool   `json:"mute"`
}
type udpNetUSBStatus struct {
	PlayTime       *int64 `json:"play_time"`
	PlayInfoUpdate *bool  `json:"play_info_updated"`
}

// Start launches a new monitor which will be running until the provided context is done.
func Start(ctx context.Context, conn *net.UDPConn, port int) *Monitor {
	m := &Monitor{
		conn:             conn,
		port:             port,
		devices:          make(map[string]knownDevices),
		jsonChan:         make(chan ([]byte), 64),
		eventSubscribers: make(map[string]SubscribeFunc),
	}
	go m.run(ctx)
	go m.handlePlayload(ctx)
	return m
}

// AddDevice starts monitoring the given device and returns its DeviceId on success.
func (m *Monitor) AddDevice(ctx context.Context, s DataSource) (string, error) {
	di, err := s.GetDeviceInfo(ctx)
	if err != nil {
		return "", err
	}

	did := di.DeviceId
	if did == "" {
		return "", fmt.Errorf("no valid device id received")
	}

	// Build json to trigger the initial update.
	trigger := struct {
		DeviceId string `json:"device_id"`
	}{
		DeviceId: did,
	}
	bootstrap, err := json.Marshal(trigger)
	if err != nil {
		return "", err
	}

	// Acquire lock: no more device requests should be made.
	m.Lock()
	defer m.Unlock()
	if _, ok := m.devices[did]; ok {
		return "", fmt.Errorf("device %q is already monitored", did)
	}

	dctx, dcancel := context.WithCancel(ctx)
	m.devices[did] = knownDevices{
		src:    s,
		cancel: dcancel,
	}

	// Start subscribe keep-alive loop; canceled by calling 'dcancel'.
	go func() {
		// Send a fake message to the json receiver to trigger an initial update.
		m.jsonChan <- bootstrap
		for {
			s.SubscribeEvents(dctx, m.port)
			select {
			case <-time.After(subscribeTtl):
				// continue
			case <-dctx.Done():
				return
			}
		}
	}()

	return did, nil
}

// RemoveDevice wipes a device from the monitored pool.
func (m *Monitor) RemoveDevice(ctx context.Context, did string) error {
	m.Lock()
	defer m.Unlock()

	kd, ok := m.devices[did]
	if !ok {
		return fmt.Errorf("device %q was not registered", did)
	}
	kd.cancel()
	delete(m.devices, did)
	return nil
}

// run executed the main loop, reading data from the provided UDP socket and triggering device state updates.
func (m *Monitor) run(ctx context.Context) {
	buf := make([]byte, 8192)
	for ctx.Err() == nil {
		nr, err := m.conn.Read(buf)
		if err != nil {
			continue
		}
		m.jsonChan <- buf[0:nr]
	}
}

func (m *Monitor) handlePlayload(ctx context.Context) {
	for {
		var buf []byte
		select {
		case <-ctx.Done():
			return
		case buf = <-m.jsonChan:
		}

		fmt.Printf(">>> %s\n", buf)
		s := &udpDeviceStatus{}
		err := json.Unmarshal(buf, s)
		if err != nil {
			fmt.Printf("Err: %+v\n", err)
			continue
		}
		did := s.DeviceID
		if len(did) == 0 {
			fmt.Printf("No device id?!")
			continue
		}

		pu, err := m.updateFromUdp(did, s)
		if err != nil {
			fmt.Printf("updateFromUdp err: %w\n", err)
			continue
		}
		if pu {
			if err := m.updatePlayInfo(ctx, did); err != nil {
				fmt.Printf("updatePlayInfo err: %w\n", err)
				continue
			}
		}
		if err := m.broadcastChange(did); err != nil {
			fmt.Printf("broadcast failure for %q: %w\n", did, err)
		}
	}
}

// broadcastChange distributes new status information to receivers.
func (m *Monitor) broadcastChange(did string) error {
	m.RLock()
	defer m.RUnlock()

	pl, ok := m.devices[did]
	if !ok {
		return fmt.Errorf("nothing to broadcast for %q", did)
	}

	for _, fn := range m.eventSubscribers {
		fn(did, pl.src.GetIP(), pl.status)
	}
	return nil
}

func (m *Monitor) broadcastCurrentState(fn SubscribeFunc) {
	m.RLock()
	defer m.RUnlock()

	for did, pl := range m.devices {
		fn(did, pl.src.GetIP(), pl.status)
	}
}

// updatePlayInfo fetches device information using TCP.
func (m *Monitor) updatePlayInfo(ctx context.Context, did string) error {
	var src DataSource

	m.RLock()
	kdev, ok := m.devices[did]
	if ok {
		src = kdev.src
	}
	m.RUnlock()

	if !ok {
		return fmt.Errorf("device is not registered")
	}

	pi, err := src.GetPlayInfo(ctx)
	if err != nil {
		return err
	}

	si, err := src.GetStatus(ctx)
	if err != nil {
		return err
	}

	m.Lock()
	defer m.Unlock()

	kdev, ok = m.devices[did]
	if !ok {
		return fmt.Errorf("device %q vanished during update", did)
	}

	kdev.status.ArtworkURL = pi.AlbumartURL
	kdev.status.Power = si.Power
	kdev.status.Input = pi.Input
	kdev.status.Mute = si.Mute
	kdev.status.Volume = si.Volume
	kdev.status.Track = pi.Track
	kdev.status.Album = pi.Album
	kdev.status.Artist = pi.Artist
	kdev.status.Playback = pi.Playback
	kdev.status.PlayTime = pi.PlayTime
	kdev.status.Repeat = pi.Repeat
	kdev.status.RepeatAvailable = pi.RepeatAvailable
	kdev.status.Shuffle = pi.Shuffle
	kdev.status.ShuffleAvailable = pi.ShuffleAvailable
	kdev.status.SeekForwardStep = pi.SeekForwardStep
	kdev.status.SeekBackwardStep = pi.SeekBackwardStep
	m.devices[did] = kdev
	return nil
}

func (m *Monitor) updateFromUdp(did string, s *udpDeviceStatus) (bool, error) {
	m.Lock()
	defer m.Unlock()

	kdev, ok := m.devices[did]
	if !ok {
		return false, fmt.Errorf("device not regiestred")
	}

	var needsPlayUpdate bool

	if kdev.status.Power == "" {
		needsPlayUpdate = true
	}
	if m := s.Main; m != nil {
		if m.Mute != nil {
			kdev.status.Mute = *m.Mute
		}
		if m.Power != nil {
			kdev.status.Power = *m.Power
		}
		if m.Volume != nil {
			kdev.status.Volume = *m.Volume
		}
	}
	if n := s.NetUSB; n != nil {
		if u := n.PlayInfoUpdate; u != nil && *u == true {
			needsPlayUpdate = true
		}
		if p := n.PlayTime; p != nil {
			kdev.status.PlayTime = *p
		}
	}
	m.devices[did] = kdev
	return needsPlayUpdate, nil
}
