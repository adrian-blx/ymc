package monitor

import (
	"fmt"
	"math/rand"
)

type SubscribeFunc func(did, ip string, s Status)

// Subscribe subscribes the given function to receive device state updates.
// Returns a token which must be used to call Unsubscribe.	fmto
func (m *Monitor) Subscribe(fn SubscribeFunc) (string, error) {
	m.Lock()
	defer m.Unlock()

	token := fmt.Sprintf("<sub:%v-%d>", fn, rand.Int63())
	if _, ok := m.eventSubscribers[token]; ok {
		return "", fmt.Errorf("token %s is already subscribed", token)
	}
	m.eventSubscribers[token] = fn
	// make sure new subscriber gets a timely update.
	go m.broadcastCurrentState(fn)
	return token, nil
}

// Unsubscribe returns the given token (returned previously by Subscribe()) from the pool of notified functions.
func (m *Monitor) Unsubscribe(token string) error {
	m.Lock()
	defer m.Unlock()

	if _, ok := m.eventSubscribers[token]; !ok {
		return fmt.Errorf("token %q was never subscribed", token)
	}
	delete(m.eventSubscribers, token)
	return nil
}
