package controller

import (
	"context"
	"encoding/json"
	"fmt"
)

type BluetoothInfo struct {
	ResponseCode int             `json:"response_code"`
	Standby      bool            `json:"bluetooth_standby"`
	TxSetting    bool            `json:"bluetooth_tx_setting"`
	Device       BluetoothDevice `json:"bluetooth_device"`
}

type BluetoothDeviceList struct {
	ResponseCode int               `json:"response_code"`
	Updating     bool              `json:"updating"`
	DeviceList   []BluetoothDevice `json:"device_list"`
}

type BluetoothDevice struct {
	Connected bool   `json:"connected"`
	Name      string `json:"name"`
	Type      string `json:"type"`
	Address   string `json:"address"`
}

// GetBluetoothInfo returns the current BT operation mode of the device.
func (c *Controller) GetBluetoothInfo(ctx context.Context) (*BluetoothInfo, error) {
	msg, err := c.srv.SendControl(ctx, "v1/system/getBluetoothInfo")
	if err != nil {
		return nil, err
	}
	r := &BluetoothInfo{}
	if err := json.Unmarshal(msg, r); err != nil {
		return nil, err
	}
	if r.ResponseCode != 0 {
		return nil, fmt.Errorf("unexpected response code: %d", r.ResponseCode)
	}
	return r, nil
}

// GetBluetoothDeviceList returns a list of known devices.
func (c *Controller) GetBluetoothDeviceList(ctx context.Context) (*BluetoothDeviceList, error) {
	msg, err := c.srv.SendControl(ctx, "v1/system/getBluetoothDeviceList")
	if err != nil {
		return nil, err
	}
	r := &BluetoothDeviceList{}
	if err := json.Unmarshal(msg, r); err != nil {
		return nil, err
	}
	if r.ResponseCode != 0 {
		return nil, fmt.Errorf("unexpected response code: %d", r.ResponseCode)
	}
	return r, nil
}

// BluetoothConnectDevice attempts to connect to the specified addr.
func (c *Controller) BluetoothConnectDevice(ctx context.Context, addr string) error {
	if len(addr) != 12 {
		return fmt.Errorf("BT address expected to be 12 chars long, %q is %d", addr, len(addr))
	}
	return c.simpleGet(ctx, fmt.Sprintf("v1/system/connectBluetoothDevice?address=%s", addr))
}

// BluetoothDisconnectDevice terminates the connection with the currently connected device.
func (c *Controller) BluetoothDisconnectDevice(ctx context.Context) error {
	return c.simpleGet(ctx, "v1/system/disconnectBluetoothDevice")
}

// BluetoothUpdateDeviceList updates the BT device list.
func (c *Controller) BluetoothUpdateDeviceList(ctx context.Context) error {
	return c.simpleGet(ctx, "v1/system/updateBluetoothDeviceList")
}

// SetBluetoothStandby enables or disables the BT standby functionality.
func (c *Controller) SetBluetoothStandby(ctx context.Context, enable bool) error {
	return c.simpleGet(ctx, fmt.Sprintf("v1/system/setBluetoothStandby?enable=%t", enable))
}

// SetBluetoothTx enables or disables BT transmission.
func (c *Controller) SetBluetoothTx(ctx context.Context, enable bool) error {
	return c.simpleGet(ctx, fmt.Sprintf("v1/system/setBluetoothTxSetting?enable=%t", enable))
}
