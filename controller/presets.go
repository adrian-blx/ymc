package controller

import (
	"context"
	"encoding/json"
	"fmt"
)

type PresetContainer struct {
	ResponseCode int          `json:"response_code"`
	PresetInfo   []PresetInfo `json:"preset_info"`
}

type PresetInfo struct {
	Url   string `json:"url,omitempty"`
	Input string `json:"input,omitempty"`
	Text  string `json:"text"`
}

// GetPresets returns the presets (favourites) stored on the device.
func (c *Controller) GetPresets(ctx context.Context) (*PresetContainer, error) {
	msg, err := c.srv.SendControl(ctx, "v1/netusb/getPresetInfo")
	if err != nil {
		return nil, err
	}
	r := &PresetContainer{}
	if err := json.Unmarshal(msg, r); err != nil {
		return nil, err
	}

	return r, nil
}

// ActivatePreset activates the specified preset index (1 based!).
func (c *Controller) ActivatePreset(ctx context.Context, num int) error {
	return c.simpleGet(ctx, fmt.Sprintf("v1/netusb/recallPreset?zone=%s&num=%d", c.zone, num))
}
