package controller

import (
	"context"
	"encoding/json"
	"fmt"
)

// simpleGet executes a standard control GET message and checks the response_code.
func (c *Controller) simpleGet(ctx context.Context, path string) error {
	msg, err := c.srv.SendControl(ctx, path)
	if err != nil {
		return err
	}
	r := &struct {
		ResponseCode int `json:"response_code"`
	}{}
	if err := json.Unmarshal(msg, r); err != nil {
		return err
	}
	if r.ResponseCode != 0 {
		return fmt.Errorf("unexpected response code: %d", r.ResponseCode)
	}
	return nil
}
