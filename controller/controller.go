package controller

import (
	"context"
)

type Server interface {
	SendControl(ctx context.Context, path string) ([]byte, error)
	SendSubscribeUDP(ctx context.Context, port int) error
	SendUpnp(ctx context.Context, action string, payload []byte) error
	GetBlob(ctx context.Context, kind, key string) ([]byte, error)
	IP() string
}

type Controller struct {
	zone string
	srv  Server
}

// New returns a new controller using the specified Zone and Server.
func New(zone string, srv Server) *Controller {
	return &Controller{
		zone: zone,
		srv:  srv,
	}
}
