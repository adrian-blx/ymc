package controller

import (
	"context"
	"encoding/json"
)

type DeviceInfo struct {
	ResponseCode  int     `json:"response_code"`
	ModelName     string  `json:"model_name"`
	RegionCode    string  `json:"destination"`
	DeviceId      string  `json:"device_id"`
	SystemVersion float32 `json:"system_version"`
	SystemId      string  `json:"system_id"`
}

type Features struct {
	ResponseCode int            `json:"response_code"`
	System       SystemFeatures `json:"system"`
	Zone         []SystemZone   `json:"zone"`
	Clock        SystemClock    `json:"clock"`
}

type SystemFeatures struct {
	FunctionList []string          `json:"func_list"`
	ZoneNum      int64             `json:"zone_num"`
	InputList    []SystemInput     `json:"input_list"`
	RangeStep    []SystemRangeStep `json:"range_step"`
}

type SystemInput struct {
	Id                 string `json:"id"`
	DistributionEnable bool   `json:"distribution_enable"`
	RenameEnabled      bool   `json:"rename_enabled"`
	AccountEnabled     bool   `json:"account_enabled"`
	PlayInfoType       string `json:"string"`
}

type SystemRangeStep struct {
	Id   string `json:"id"`
	Min  int64  `json:"min"`
	Max  int64  `json:"max"`
	Step int64  `json:"step"`
}

type SystemZone struct {
	Id                string            `json:"id"`
	FunctionList      []string          `json:"func_list"`
	InputList         []string          `json:"input_list"`
	SoundProgramList  []string          `json:"sound_program_list"`
	EqualizerModeList []string          `json:"equalizer_mode_list"`
	LinkControlList   []string          `json:"link_control_list"`
	RangeStep         []SystemRangeStep `json:"range_step"`
}

type SystemClock struct {
	FunctionList    []string          `json:"func_list"`
	RangeStep       []SystemRangeStep `json:"range_step"`
	AlarmInputList  []string          `json:"alarm_input_list"`
	AlarmPresetList []string          `json:"alarm_preset_list"`
}

type NetworkStatus struct {
	NetworkName string `json:"network_name"`
	Connection  string `json:"connection"`
	IP          string `json:"ip_address"`
	Netmask     string `json:"subnet_mask"`
	Gateway     string `json:"default_gateway"`
	DNS1        string `json:"dns_server_1"`
	DNS2        string `json:"dns_server_2"`
}

// GetDeviceInfo returns discovered information about the currently connected device.
func (c *Controller) GetDeviceInfo(ctx context.Context) (*DeviceInfo, error) {
	msg, err := c.srv.SendControl(ctx, "v1/system/getDeviceInfo")
	if err != nil {
		return nil, err
	}
	r := &DeviceInfo{}
	if err := json.Unmarshal(msg, r); err != nil {
		return nil, err
	}

	return r, nil
}

func (c *Controller) GetFeatures(ctx context.Context) (*Features, error) {
	msg, err := c.srv.SendControl(ctx, "v1/system/getFeatures")
	if err != nil {
		return nil, err
	}
	r := &Features{}
	if err := json.Unmarshal(msg, r); err != nil {
		return nil, err
	}

	return r, nil
}

func (c *Controller) GetNetworkStatus(ctx context.Context) (*NetworkStatus, error) {
	msg, err := c.srv.SendControl(ctx, "v1/system/getNetworkStatus")
	if err != nil {
		return nil, err
	}
	r := &NetworkStatus{}
	if err := json.Unmarshal(msg, r); err != nil {
		return nil, err
	}

	return r, nil
}

// GetIP returns the current device IP.
func (c *Controller) GetIP() string {
	return c.srv.IP()
}
