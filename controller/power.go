package controller

import (
	"context"
	"fmt"
)

// PowerOn turns the device on.
func (c *Controller) PowerOn(ctx context.Context) error {
	return c.powerSet(ctx, "on")
}

// PowerOff puts the device in standby mode.
func (c *Controller) PowerOff(ctx context.Context) error {
	return c.powerSet(ctx, "standby")
}

// PowerToggle toggles the standby state.
func (c *Controller) PowerToggle(ctx context.Context) error {
	return c.powerSet(ctx, "toggle")
}

func (c *Controller) powerSet(ctx context.Context, what string) error {
	return c.simpleGet(ctx, fmt.Sprintf("v1/%s/setPower?power=%s", c.zone, what))
}
