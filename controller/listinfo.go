package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
)

type ListInfoReply struct {
	MenuName string         `json:"menu_name"`
	Input    string         `json:"input"`
	Layer    int32          `json:"menu_layer"`
	Pages    int32          `json:"pages"`
	Items    []ListInfoItem `json:"items"`
}

type ListInfoItem struct {
	Index      int32  `json:"index"`
	Text       string `json:"text"`
	Thumbnail  string `json:"thumbnail"`
	Playable   bool   `json:"playable"`
	Selectable bool   `json:"selectable"`
	Searchable bool   `json:"searchable"`
	NowPlaying bool   `json:"now_playing"`
}

type yamaListInfo struct {
	MenuName     string             `json:"menu_name"`
	Input        string             `json:"input"`
	MenuLayer    int32              `json:"menu_layer"`
	MaxLine      int32              `json:"max_line"`
	Index        int32              `json:"index"`
	PlayingIndex int32              `json:"playing_index"`
	Items        []yamaListInfoItem `json:"list_info"`
}

type yamaListInfoItem struct {
	Text      string `json:"text"`
	Thumbnail string `json:"thumbnail"`
	Attribute uint32 `json:"attribute"`
}

type listControlType string

const (
	ListControlSelect    = listControlType("select")
	ListControlPlay      = listControlType("play")
	ListControlReturn    = listControlType("return")
	ListControlPeek      = listControlType("peek")
	listControlChunkSize = 8 // only 8 is supported by music cast devices.
	listControlPageSize  = listControlChunkSize * 2
)

func (c *Controller) ListInfo(ctx context.Context, action listControlType, index, page int32, source inputType) (*ListInfoReply, error) {
	if action != ListControlPeek {
		_, err := c.srv.SendControl(ctx, fmt.Sprintf("v1/netusb/setListControl?type=%s&index=%d&zone=%s", action, index, c.zone))
		if err != nil {
			return nil, err
		}
	}
	if action == ListControlPlay {
		return nil, nil
	}

	var reply *ListInfoReply
	offset := listControlPageSize * page
	for {
		fmt.Printf("> Getting IDX %d\n", offset)
		pl, err := c.srv.SendControl(ctx, fmt.Sprintf("v1/netusb/getListInfo?list_id=main&input=%s&index=%d&size=%d", source, offset, listControlChunkSize))
		if err != nil {
			return nil, err
		}
		r := &yamaListInfo{}
		if err := json.Unmarshal(pl, r); err != nil {
			return nil, err
		}
		// First batch of the paginated reply: create reply struct and fill in metadata.
		if reply == nil {
			reply = &ListInfoReply{
				MenuName: r.MenuName,
				Input:    r.Input,
				Layer:    r.MenuLayer,
				Pages:    int32(math.Ceil(float64(r.MaxLine) / float64(listControlPageSize))),
			}
		}
		for i, v := range r.Items {
			e := ListInfoItem{
				Index:      offset + int32(i),
				Text:       v.Text,
				Thumbnail:  v.Thumbnail,
				Selectable: (v.Attribute & (1 << 1)) != 0,
				Playable:   (v.Attribute & (1 << 2)) != 0,
				Searchable: (v.Attribute & (1 << 3)) != 0,
				NowPlaying: (v.Attribute & (1 << 5)) != 0,
			}
			reply.Items = append(reply.Items, e)
		}
		offset += listControlChunkSize
		if offset >= r.MaxLine || len(reply.Items) >= listControlPageSize {
			break
		}
	}

	if reply == nil {
		return nil, fmt.Errorf("no data received")
	}
	return reply, nil
}
