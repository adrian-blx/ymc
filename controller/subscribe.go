package controller

import "context"

func (c *Controller) SubscribeEvents(ctx context.Context, port int) error {
	return c.srv.SendSubscribeUDP(ctx, port)
}
