package controller

import (
	"context"
	"fmt"
)

type inputType string

var (
	InputSpotify   = inputType("spotify")
	InputDeezer    = inputType("deezer")
	InputTidal     = inputType("tidal")
	InputRadio     = inputType("net_radio")
	InputServer    = inputType("server")
	InputBluetooth = inputType("bluetooth")
)

// SetInput changes the device input to the given inputType.
func (c *Controller) SetInput(ctx context.Context, input inputType) error {
	return c.simpleGet(ctx, fmt.Sprintf("v1/%s/setInput?input=%s", c.zone, input))
}
