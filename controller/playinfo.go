package controller

import (
	"context"
	"encoding/json"
	"fmt"
)

type PlayInfo struct {
	ResponseCode     int          `json:"response_code"`
	Input            string       `json:"input"`
	Playback         string       `json:"playback"`
	Repeat           string       `json:"repeat"`
	Shuffle          string       `json:"shuffle"`
	PlayTime         int64        `json:"play_time"`
	TotalTime        int64        `json:"total_time"`
	Artist           string       `json:"artist"`
	Album            string       `json:"album"`
	Track            string       `json:"track"`
	AlbumartURL      string       `json:"albumart_url"`
	AutoStopped      bool         `json:"auto_stopped"`
	SeekBackwardStep int64        `json:"seek_backward_step"`
	SeekForwardStep  int64        `json:"seek_forward_step"`
	RepeatAvailable  []string     `json:"repeat_available"`
	ShuffleAvailable []string     `json:"shuffle_available"`
	Attribute        AttributeBit `json:"attribute"`
	AttributeFlags   PlayInfoAttributes
}

type AttributeBit uint32

const (
	Playable AttributeBit = 1 << iota
	CapableOfStop
	CapableOfPause
	CapableOfPRevSkip
	CapableOfNextSkip
	CapableOfFastReverse
	CapableOfFastForward
	CapableOfRepeat
	CapableOfShuffle
	FeedbaclAvailable
	ThumbsUp
	ThumbsDown
	Video
	CapableOfBookmark
	CapableOfDRM
	StationPlayback
	ADPlayback
	SharedStation
	CapableOfAddTrack
	CapableOfAddAlbum
	ShuffleStation
	CapableOfAddChannel
	SamplePlayback
	MusicPlayPlayback
	CapableOfLinkDistribution
	CapableOfAddPlaylist
	CapableOfAddMusicCastPlaylist
)

type PlayInfoAttributes struct {
	Playable bool
	CanStop  bool
}

type ZoneStatus struct {
	ResponseCode       int    `json:"response_code"`
	Power              string `json:"power"`
	Sleep              uint32 `json:"sleep"`
	Volume             uint32 `json:"volume"`
	Mute               bool   `json:"mute"`
	Input              string `json:"input"`
	DistributionEnable bool   `json:"distribution_enable"`
}

func (c *Controller) GetPlayInfo(ctx context.Context) (*PlayInfo, error) {
	msg, err := c.srv.SendControl(ctx, "v1/netusb/getPlayInfo")
	if err != nil {
		return nil, err
	}
	r := &PlayInfo{}
	if err := json.Unmarshal(msg, r); err != nil {
		return nil, err
	}

	if r.Attribute&Playable != 0 {
		r.AttributeFlags.Playable = true
	}
	if r.Attribute&CapableOfStop != 0 {
		r.AttributeFlags.CanStop = true
	}
	// TODO: Add more decoded flags.
	return r, nil
}

func (c *Controller) GetStatus(ctx context.Context) (*ZoneStatus, error) {
	msg, err := c.srv.SendControl(ctx, fmt.Sprintf("v1/%s/getStatus", c.zone))
	if err != nil {
		return nil, err
	}
	r := &ZoneStatus{}
	if err := json.Unmarshal(msg, r); err != nil {
		return nil, err
	}
	return r, nil
}
