package controller

import (
	"context"
	"fmt"
)

// VolumeChange increases or decreases the volume by the specified amount.
func (c *Controller) VolumeChange(ctx context.Context, step int) error {
	cmd := "up"
	if step == 0 {
		return fmt.Errorf("volume step can not be zero")
	}
	if step < 0 {
		cmd = "down"
		step = step * -1
	}
	return c.simpleGet(ctx, fmt.Sprintf("v1/%s/setVolume?volume=%s&step=%d", c.zone, cmd, step))
}

// VolumeSet performs an absolute volume change.
func (c *Controller) VolumeSet(ctx context.Context, val int64) error {
	return c.simpleGet(ctx, fmt.Sprintf("v1/%s/setVolume?volume=%d", c.zone, val))
}

// VolumeMute mutes or unmutes the device.
func (c *Controller) VolumeMute(ctx context.Context, muted bool) error {
	return c.simpleGet(ctx, fmt.Sprintf("v1/%s/setMute?enable=%t", c.zone, muted))
}
