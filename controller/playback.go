package controller

import (
	"context"
	"fmt"
)

type playbackMode string

var (
	PlaybackPlay         = playbackMode("play")
	PlaybackStop         = playbackMode("stop")
	PlaybackPause        = playbackMode("pause")
	PlaybackToggle       = playbackMode("play_pause")
	PlaybackPrevious     = playbackMode("previous")
	PlaybackNext         = playbackMode("next")
	PlaybackSeekForward  = playbackMode("seek_forward")
	PlaybackSeekBackward = playbackMode("seek_backward")
)

// SetPlayback changes the controllers playback mode.
func (c *Controller) SetPlayback(ctx context.Context, mode playbackMode) error {
	return c.simpleGet(ctx, fmt.Sprintf("v1/netusb/setPlayback?playback=%s", mode))
}

// SetPlayPosition seeks in the currently playing track.
func (c *Controller) SetPlayPosition(ctx context.Context, pos int) error {
	return c.simpleGet(ctx, fmt.Sprintf("v1/netusb/setPlayPosition?position=%d", pos))
}
