package controller

import (
	"context"
	"fmt"
)

type repeatMode string
type shuffleMode string

var (
	RepeatOff     = repeatMode("off")
	RepeatOne     = repeatMode("one")
	RepeatAll     = repeatMode("all")
	ShuffleOff    = shuffleMode("off")
	ShuffleOn     = shuffleMode("on")
	ShuffleSongs  = shuffleMode("songs")
	ShuffleAlbums = shuffleMode("albums")
)

func (c *Controller) SetRepeat(ctx context.Context, mode repeatMode) error {
	return c.simpleGet(ctx, fmt.Sprintf("v1/netusb/setRepeat?mode=%s", mode))
}

func (c *Controller) SetShuffle(ctx context.Context, mode shuffleMode) error {
	return c.simpleGet(ctx, fmt.Sprintf("v1/netusb/setShuffle?mode=%s", mode))
}
