package controller

import (
	"context"
	"encoding/xml"

	"codeberg.org/adrian-blx/ymc/didl"
)

type UpnpEnvelope struct {
	XMLName       xml.Name  `xml:"s:Envelope"`
	EncodingStyle string    `xml:"s:encodingStyle,attr"`
	XmlNS         string    `xml:"xmlns:s,attr"`
	Body          *UpnpBody `xml:"s:Body,omitempty"`
}

type UpnpBody struct {
	Play           *UpnpPlay           `xml:"u:Play,omitempty"`
	AVTransportURI *UpnpAvTransportUri `xml:"u:SetAVTransportURI"`
}

type UpnpPlay struct {
	XmlNS      string `xml:"xmlns:s,attr"`
	InstanceID int    `xml:"InstanceID"`
	Speed      int    `xml:"Speed"`
}

type UpnpAvTransportUri struct {
	XmlNS              string `xml:"xmlns:s,attr"`
	InstanceID         int    `xml:"InstanceID"`
	CurrentURI         string `xml:"CurrentURI"`
	CurrentURIMetaData string `xml:"CurrentURIMetaData"`
}

const (
	upnpEncodingStyle = `http://schemas.xmlsoap.org/soap/encoding/`
	upnpEnvelopeNS    = `http://schemas.xmlsoap.org/soap/envelope/`
	upnpPlayNS        = `urn:schemas-upnp-org:service:AVTransport:1`
)

func (c *Controller) SendUpnpPlay(ctx context.Context) error {
	e := &UpnpEnvelope{
		EncodingStyle: upnpEncodingStyle,
		XmlNS:         upnpEnvelopeNS,
		Body: &UpnpBody{
			Play: &UpnpPlay{
				XmlNS:      upnpPlayNS,
				InstanceID: 0,
				Speed:      1,
			},
		},
	}
	pl, err := xml.MarshalIndent(e, "", " ")
	if err != nil {
		return err
	}
	return c.srv.SendUpnp(ctx, "Play", pl)
}

func (c *Controller) SendUpnpUrl(ctx context.Context, uri string) error {
	e := &UpnpEnvelope{
		EncodingStyle: upnpEncodingStyle,
		XmlNS:         upnpEnvelopeNS,
		Body: &UpnpBody{
			AVTransportURI: &UpnpAvTransportUri{
				XmlNS:              upnpPlayNS,
				CurrentURI:         uri,
				CurrentURIMetaData: didl.GenDidl(didl.DidlInput{
					Title: "UPNP Stream",
					Artist: "UPNP artist",
					Album: "UPNP input",
					Uri: uri,
				}),
			},
		},
	}
	pl, err := xml.MarshalIndent(e, "", " ")
	if err != nil {
		return err
	}
	return c.srv.SendUpnp(ctx, "SetAVTransportURI", pl)
}
