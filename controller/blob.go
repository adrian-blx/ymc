package controller

import "context"

func (c *Controller) GetBlob(ctx context.Context, kind, key string) ([]byte, error) {
	return c.srv.GetBlob(ctx, kind, key)
}
