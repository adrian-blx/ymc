package main

import (
	"context"
	"flag"
	"fmt"
	"net"
	"os"
	"sort"

	"codeberg.org/adrian-blx/ymc/controller"
	"codeberg.org/adrian-blx/ymc/monitor"
	"codeberg.org/adrian-blx/ymc/server"
)

var (
	flagServer = flag.String("server", "192.168.1.45", "IP of server to connect to")
	flagZone   = flag.String("zone", "main", "Zone to use")
	flagAddr   = flag.String("addr", "", "Used by subcommands")
)

type cmd struct {
	help string
	fn   func() error
}

func main() {
	flag.Parse()

	srv, err := server.NewStatic(*flagServer)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to connect to %q: %v\n", *flagServer, err)
		os.Exit(1)
	}
	ctx := context.Background()
	ctrl := controller.New(*flagZone, srv)

	cmds := map[string]*cmd{
		"on": {
			help: "Turn the device on",
			fn:   func() error { return ctrl.PowerOn(ctx) },
		},
		"off": {
			help: "Turn the device off",
			fn:   func() error { return ctrl.PowerOff(ctx) },
		},
		"play": {
			help: "Start playback",
			fn:   func() error { return ctrl.SetPlayback(ctx, controller.PlaybackPlay) },
		},
		"stop": {
			help: "Stop playback",
			fn:   func() error { return ctrl.SetPlayback(ctx, controller.PlaybackStop) },
		},
		"next": {
			help: "Switch to next track",
			fn:   func() error { return ctrl.SetPlayback(ctx, controller.PlaybackNext) },
		},
		"previous": {
			help: "Switch to previous track",
			fn:   func() error { return ctrl.SetPlayback(ctx, controller.PlaybackPrevious) },
		},
		"mute-on": {
			help: "Mute device",
			fn:   func() error { return ctrl.VolumeMute(ctx, true) },
		},
		"mute-off": {
			help: "Unmute device",
			fn:   func() error { return ctrl.VolumeMute(ctx, false) },
		},
		"vol+": {
			help: "Increase volume",
			fn:   func() error { return ctrl.VolumeChange(ctx, +1) },
		},
		"vol-": {
			help: "Decrease volume",
			fn:   func() error { return ctrl.VolumeChange(ctx, -1) },
		},
		"info": {
			help: "Dump device info",
			fn: func() error {
				d, err := ctrl.GetDeviceInfo(ctx)
				fmt.Printf("%+v\n", d)
				return err
			},
		},
		"play-info": {
			help: "Dump play info",
			fn: func() error {
				d, err := ctrl.GetPlayInfo(ctx)
				fmt.Printf("%+v\n", d)
				return err
			},
		},
		"features": {
			help: "Dump device features",
			fn: func() error {
				d, err := ctrl.GetFeatures(ctx)
				fmt.Printf("%+v\n", d)
				return err
			},
		},
		"bluetooth-info": {
			help: "Show bluetooth information",
			fn: func() error {
				r, err := ctrl.GetBluetoothInfo(ctx)
				fmt.Printf("%+v\n", r)
				return err
			},
		},
		"bluetooth-device-list": {
			help: "Show known bluetooth devices",
			fn: func() error {
				r, err := ctrl.GetBluetoothDeviceList(ctx)
				fmt.Printf("%+v\n", r)
				return err
			},
		},
		"bluetooth-connect-device": {
			help: "Connect to device specified in -addr flag",
			fn: func() error {
				return ctrl.BluetoothConnectDevice(ctx, *flagAddr)
			},
		},
		"bluetooth-disconnect": {
			help: "Disconnect the currently connected BT speaker",
			fn: func() error {
				return ctrl.BluetoothDisconnectDevice(ctx)
			},
		},
		"bluetooth-update-devices": {
			help: "Updates the devices BT device list",
			fn: func() error {
				return ctrl.BluetoothUpdateDeviceList(ctx)
			},
		},
		"bluetooth-tx-on": {
			help: "Turns BT on",
			fn: func() error {
				return ctrl.SetBluetoothTx(ctx, true)
			},
		},
		"bluetooth-tx-off": {
			help: "Turns BT off",
			fn: func() error {
				return ctrl.SetBluetoothTx(ctx, false)
			},
		},
		"monitor": {
			help: "block and report status updates",
			fn: func() error {
				port := 11555
				conn, err := net.ListenUDP("udp", &net.UDPAddr{Port: port})
				if err != nil {
					panic(err)
				}
				m := monitor.Start(ctx, conn, port)
				m.AddDevice(ctx, ctrl)
				fnx := func(id, ip string, s monitor.Status) {
					fmt.Printf("######> %s (%s)\t%+v\n", id, ip, s)
				}
				m.Subscribe(fnx)
				<-ctx.Done()
				return nil
			},
		},
		"status": {
			help: "Report Zone status",
			fn: func() error {
				r, err := ctrl.GetStatus(ctx)
				fmt.Printf("> %+v\n", r)
				return err
			},
		},
		"upnp": {
			help: "Play upnp data",
			fn: func() error {
				if len(flag.Args()) <= 1 {
					return fmt.Errorf("upnp needs an URL as argument")
				}
				what := flag.Args()[1]
				if err := ctrl.SendUpnpUrl(ctx, what); err != nil {
					return err
				}
				return ctrl.SendUpnpPlay(ctx)
			},
		},
	}

	for _, cmd := range flag.Args() {
		c, ok := cmds[cmd]
		if !ok {
			fmt.Fprintf(os.Stderr, "unknown command: %q\n", cmd)
			continue
		}
		fmt.Printf("# %q\n", cmd)
		if err := c.fn(); err != nil {
			fmt.Fprintf(os.Stderr, fmt.Sprintf("subcommand %q exited with error: %s\n", cmd, err))
		}
	}

	if len(flag.Args()) == 0 {
		var keys []string
		for k := range cmds {
			keys = append(keys, k)
		}
		sort.Strings(keys)
		for _, k := range keys {
			fmt.Printf("%-27s%s\n", k, cmds[k].help)
		}
	}
}
