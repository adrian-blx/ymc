# YMC - Control Yamaha Music Cast compatible devices

This repository controlls a library and tools to control devices compatible with the music cast protocol.

Functionality is regularily tested with the following models.

* [MusicCast 20](https://de.yamaha.com/de/products/audio_visual/desktop_audio/musiccast_20/index.html)
* [WXAD-10](https://de.yamaha.com/de/products/audio_visual/accessories/wxad-10/index.html)

Other devices very likely work out of the box, but certain functionality (especially everything not supported by the devices listen above) is likely to be not implemented.

## Usage

### `yama.go`

`yama.go` is a simple CLI to interact with musiccast devices. It is mostly used to demonstrate and test the use of the library code, so don't expect its usage to be stable.

Note that device discovery is not supported, you are expected to supply the IP of the device you want to interact with using the `-server` flag.

Example usage:

```
$ go run yama.go -server 192.168.1.6 info
```

### `yweb.go`

`yweb.go` is my reason for creating this git repository :-)

It provides a simple, yet functional, web interface to control music cast devices from any moderately modern browser.

Keep in mind that `yweb.go` does not support device discovery, so you will need to supply the IP of the device(s) you want to control using the `-clients` flag.
Note that multiple devices are supported by seperating ther IP using a `,`.

Example usage to start the server for 2 devices and have it listen on `127.0.0.1:8012`:

```
$ go run yweb.go -clients 192.168.1.6,192.168.1.7 -listen 127.0.0.1:8012
```

Once the program launched, you would be able to access the web interface by navigating to http://127.0.0.1:8012

Note: you likely need to turn the device on (using the power button on the upper right corner) for most functions to work.

![webinterface](https://www.blinkenlights.ch/static/yweb.png)
