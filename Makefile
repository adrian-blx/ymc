export GO111MODULE=on
PRODUCT := "yweb"

clean:
	rm -rf bin bundle.tgz

bundle.tgz: bin/$(PRODUCT)_linux_amd64 bin/$(PRODUCT)_linux_arm64 bin/$(PRODUCT)_linux_arm
	tar -czvf $@ bin/$(PRODUCT)_*

publish: bundle.tgz
	OUT=$(shell sha1sum $< | awk '{print $$1 ".tgz"}'); \
	mv $< $$OUT && rclone copy $$OUT b2:blx-public/nomad/$(PRODUCT)/ && \
	rm $$OUT

bin/$(PRODUCT)_%:
	CGO_ENABLED=0 GOOS=linux GOARCH=$(subst bin/$(PRODUCT)_linux_,,$@) go build -o $@ $(PRODUCT).go
