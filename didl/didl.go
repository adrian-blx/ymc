package didl

import (
	"encoding/xml"
)

const (
	didlNS   = "urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/"
	didlUpnp = "urn:schemas-upnp-org:metadata-1-0/upnp/"
	didlDc   = "http://purl.org/dc/elements/1.1/"
	didlDlna = "urn:schemas-dlna-org:metadata-1-0/"
)

type DidlInput struct {
	Uri    string
	Title  string
	Artist string
	Album  string
}

type DidlLite struct {
	XMLName xml.Name `xml:"DIDL-Lite"`
	XmlNS   string   `xml:"xmlns,attr"`
	NsUpnp  string   `xml:"xmlns:upnp,attr"`
	NsDc    string   `xml:"xmlns:dc,attr"`
	NsDlna  string   `xml:"xmlns:dlna,attr"`
	Item    DidlItem `xml:"item"`
}

type DidlItem struct {
	Title       string  `xml:"dc:title,omitempty"`
	Artist      string  `xml:"upnp:artist,omitempty"`
	Album       string  `xml:"upnp:album,omitempty"`
	AlbumArtUri string  `xml:"upnp:albumArtURI,omitempty"`
	Res         DidlRes `xml:"res"`
}

type DidlRes struct {
	Size      int    `xml:"size,attr,omitempty"`
	Duration  string `xml:"duration,attr,omitempty"`
	ProtoInfo string `xml:"protocolInfo,attr"`
	Uri       string `xml:",chardata"`
}

func GenDidl(di DidlInput) string {
	e := &DidlLite{
		XmlNS:  didlNS,
		NsUpnp: didlUpnp,
		NsDc:   didlDc,
		NsDlna: didlDlna,
		Item: DidlItem{
			Title:  di.Title,
			Artist: di.Artist,
			Album:  di.Album,
			Res: DidlRes{
				ProtoInfo: "http-get:*:audio/mpeg:DLNA.ORG_PN=MP3;DLNA.ORG_OP=01;DLNA.ORG_FLAGS=01700000000000000000000000000000",
				Uri:       di.Uri,
			},
		},
	}
	pl, _ := xml.MarshalIndent(e, "", "")
	return string(pl)
}
